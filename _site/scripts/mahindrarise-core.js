(function($) {
    "use strict"; // Start of use strict
    
    $('.left_section1').mCustomScrollbar();
	$(".main-advanced-grid, .folder-list-section").mCustomScrollbar({
    theme:"light-thin"
});
	$('.dateselect, .date').datepicker();
	$('.dropdown-menu').on('click', function(event) {
        event.stopPropagation();
    });
    
    $('[data-toggle="tooltip"]').tooltip();
	// Add slideDown animation to Bootstrap dropdown when expanding.
    $('.dropdown').on('show.bs.dropdown', function() {
	   $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
    });

    // Add slideUp animation to Bootstrap dropdown when collapsing.
    $('.dropdown').on('hide.bs.dropdown', function() {
	   $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
    });
    // ===== Scroll to Top ====
	$(window).scroll(function() {
		if ($(this).scrollTop() >= 50) {
			$('#return-to-top').fadeIn(200);
		} else {
			$('#return-to-top').fadeOut(200);
		}
	});
	$('#return-to-top').click(function() {
		$('body,html').animate({
			scrollTop : 0
		}, 500);
	});
   
    jQuery(function($) {
        
        //Custom scrollbar
        $('.modal-scroll,.card-scroller,.tab-scroller,.leaderboard-scroller, .child_menu,.reference-height').mCustomScrollbar();
        
        $('.datepicker').datepicker();

		/*******************************
		* ACCORDION WITH TOGGLE ICONS
		*******************************/
		function toggleIcon(e) {
			$(e.target)
				.prev('.panel-heading')
				.find(".more-less")
				.toggleClass('fa-chevron-down fa-chevron-up');
		}
		$('.panel-group').on('hidden.bs.collapse', toggleIcon);
		$('.panel-group').on('shown.bs.collapse', toggleIcon);
		 

    });
})(jQuery); // End of use strict


		var bodywidth = $('body').width();
		if(bodywidth > 300 && bodywidth < 991){	
			$('.top-search-mob a').click(function() {
				$('.top-search-grid').slideDown();
				$('.key-search').appendTo('.top-search-grid');
			});
			$('body').mouseup(function (e) {
				var showSearchbx = $('.top-search-grid');
				if(!showSearchbx.is(e.target) && showSearchbx.has(e.target).length === 0){
					showSearchbx.slideUp();
				}
			});
			$(".closeSearchTab button").click(function(e){
				var showSearchbx = $('.top-search-grid');
				showSearchbx.slideUp();
			})
		}

